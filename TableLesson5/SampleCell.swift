//
//  SampleCell.swift
//  TableLesson5
//
//  Created by Кошенкова Мария Витальевна on 01/10/2019.
//  Copyright © 2019 Кошенкова Мария Витальевна. All rights reserved.
//

import UIKit

struct SampleCellViewModel {
    let name: String
    let image: UIImage
}

class SampleCell: UITableViewCell {

    @IBOutlet private weak var myImage: UIImageView!
    @IBOutlet private weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        myImage.layer.cornerRadius = myImage.bounds.width/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(_ viewMode:SampleCellViewModel) {
        label.text = viewMode.name
        myImage.image = viewMode.image
    }
    
}
