//
//  ViewController.swift
//  TableLesson5
//
//  Created by Кошенкова Мария Витальевна on 01/10/2019.
//  Copyright © 2019 Кошенкова Мария Витальевна. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    
    private let persons:[SampleCellViewModel] = [
        SampleCellViewModel(name: "Алексей", image: UIImage(imageLiteralResourceName: "newImage")),
        SampleCellViewModel(name: "Игнатий", image: UIImage(imageLiteralResourceName: "image_001")),
        SampleCellViewModel(name: "Аристарх", image: UIImage(imageLiteralResourceName: "0_X2-yHGibmWx5pAWN"))
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.register(UINib(nibName: "SampleCell", bundle: nil), forCellReuseIdentifier: "cell1")
    }

}

extension ViewController:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return persons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "cell1")
        //let lable = cell?.viewWithTag(3) as? UILabel
        //lable?.text="нет не был"
        //return cell!
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as? SampleCell
        cell?.configureCell(persons[indexPath.row])
        return cell!
    }
}

extension ViewController:UITableViewDelegate {
    func ta
}

